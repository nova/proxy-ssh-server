#!/bin/bash
set -euo pipefail

export SSH_USER=nova-$NOVA_USERNAME-proxy
adduser -DHs /bin/false $SSH_USER
echo "$SSH_USER:$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 50 ; echo '')" | chpasswd
cat /data/sshd_config \
	| sed 's/nova-user-proxy/'$SSH_USER'/g' \
	| sed 's/Port 4000/Port '$SSH_PORT'/' \
	| sed 's/PermitListen 8000/PermitListen '$FWD_HOST':'$FWD_PORT'/' \
	> /etc/ssh/sshd_config

ssh-keygen -A
export HOST_HASH=$(cat /etc/ssh/ssh_host_ed25519_key.pub | awk '{ print $2 }' | base64 -d | sha256sum | awk '{ print $1 }')

json_template="{ \
  host: env.SSH_HOST, \
  port: env.SSH_PORT, \
  username: env.SSH_USER, \
  forwardHost: env.FWD_HOST, \
  forwardPort: env.FWD_PORT, \
  hostKeySha256: env.HOST_HASH \
}"

asmux_data=$(jq -n "$json_template")
asmux_url="$ASMUX_HOST/_matrix/asmux/user/$NOVA_USERNAME/proxy"
asmux_hdr="Authorization: Bearer $ASMUXBEARER"

echo "Sending $asmux_data to asmux"
pubkey=$(curl --fail -XPUT "$asmux_url" -H "$asmux_hdr" -d "$asmux_data" | jq -r '.ssh.publicKey')

echo "Got user SSH public key: $pubkey"
echo "$pubkey" > /etc/ssh/authorized_keys
chmod 755 /etc/ssh
chmod 644 /etc/ssh/authorized_keys

echo "Starting sshd"
exec /usr/sbin/sshd -D -f /etc/ssh/sshd_config
