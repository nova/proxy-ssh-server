# proxy-ssh-server
Docker image for a SSH server that lets nova-desktop expose a SOCKS5 proxy to bridges.

## Environment
* `ASMUX_HOST` (defaults to `https://asmux.nova.chat`) is the asmux hostname to use.
* `ASMUX_TOKEN` is the asmux access token to use. The user-specific token is sufficient.
* `ASMUX_USER` is the Matrix username localpart (e.g. `novatester1`).
* `SSH_HOST` and `SSH_PORT` are the host and port that sshd will listen on.
* `FWD_HOST` and `FWD_PORT` (defaults to 127.0.0.1:8000) are the host and port that will be forwarded from the server to client.
